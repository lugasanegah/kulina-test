<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Messages Language Lines
    |--------------------------------------------------------------------------
    */

    'store_success_review' 			=> 'Data Review has been saved.',
    'store_failed_review' 			=> 'Failed saving data review.',
    'update_success_review' 		=> 'Data Review has been updated.',
    'update_failed_review' 			=> 'Failed updating data review.',
    'delete_user_review_success'	=> 'User Review has been deleted.',
    'delete_user_review_failed'		=> 'Failed to delete User Review.',

];