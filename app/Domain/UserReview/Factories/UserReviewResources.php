<?php

namespace App\Domain\UserReview\Factories;

use Illuminate\Http\Resources\Json\JsonResource;

class UserReviewResources extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'        => $request['id'],
            'order_id'  => $request['order_id'],
            'user_id'   => $request['user_id'],
            'rating'    => $request['rating'],
            'review'    => $request['review']

        ];
    }
}
