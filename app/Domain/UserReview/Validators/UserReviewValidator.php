<?php

namespace App\Domain\UserReview\Validators;

use App\Infrastructure\Shared\BaseValidator;

class UserReviewValidator extends BaseValidator
{
    /**
     * UserReviewValidator constructor
     */
    public function __construct()
    {
        /**
         * @var array
         */
        $this->rules = [
            'rating'        =>  'required|numeric|between:0,5',
            'order_id'      =>  'required',
            'product_id'    =>  'required',
            'user_id'       =>  'required',
        ];
    }
}
