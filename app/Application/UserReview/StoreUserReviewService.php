<?php
namespace App\Application\UserReview;

use App\Domain\UserReview\UserReviewRepositoryInterface;
use App\Domain\UserReview\Entities\UserReview;

class StoreUserReviewService
{

	protected $repository;

	public function __construct(UserReviewRepositoryInterface $repository)
	{
		$this->repository = $repository;
	}

	public function call($data, $id = null)
	{
		$review = ( $id == null ) ? new UserReview() : UserReview::find($id);

		$review->order_id	= $data['order_id'];
		$review->product_id	= $data['product_id'];
		$review->user_id	= $data['user_id'];
		$review->rating 	= $data['rating'];
		$review->review 	= $data['review'];
		
		$reviewUser = $this->repository->store($review);

		if($reviewUser){
			return [
				'status' => true, 
				'message' => ( $id == null ) ? trans('message.store_success_review') : trans('message.update_success_review'), 
				'data' => $review
			];	
		} else {
			return [
				'status' => false, 
				'message' => ( $id == null ) ? trans('message.store_failed_review') : trans('message.update_failed_review')
			];
		}
		
	}
}