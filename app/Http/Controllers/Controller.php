<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


     /**
     * @param array $errors
     * @return \Illuminate\Http\JsonResponse
     * code '001' for code error validation
     */
    public function apiUnprocessableEntityResponse($errors = [])
    {
        return apiResponseBuilder(422, [
            'errors' => [
                'code' => '001',
                'messages' => $errors
            ]
        ]);
    }

    /**
     * @param array $errors
     * @return \Illuminate\Http\JsonResponse
     * code '000' for code internal server errors
     */
    public function apiInternalServerErrorResponse($errors = [])
    {
        return apiResponseBuilder(500, [
            'errors' => [
                'code' => '000',
                'messages' => $errors
            ]
        ]);
    }
}
