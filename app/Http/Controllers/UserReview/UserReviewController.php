<?php

namespace App\Http\Controllers\UserReview;

use App\Domain\UserReview\UserReviewRepositoryInterface;
use App\Application\UserReview\StoreUserReviewService;
use App\Domain\UserReview\Validators\UserReviewValidator;
use App\Domain\UserReview\Factories\UserReviewResources;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserReviewController extends Controller
{

    protected $repository;

    public function __construct(UserReviewRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {       
        $page = ($request->has('paginate')) ? $request->get('paginate') : 9;

        $result =  $this->repository->allWithPagination($page);
        return apiResponseBuilder(200, $result);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, UserReviewValidator $validation, StoreUserReviewService $storeUserReviewService)
    {        
        $data = $request->json()->all();
        $validator = $validation->validate($data);

        if ($validator === true) {
            $result = $storeUserReviewService->call($data);
            if ($result['status']) {
                return apiResponseBuilder(200, $result);
            } else {
                return $this->apiInternalServerErrorResponse($result['message']);    
            }
        } else {
            return $this->apiUnprocessableEntityResponse($validator->all());    
        }        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserReview  $userReview
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data =  $this->repository->findById($id);

        $result =  (new UserReviewResources($data))->toArray($data);
        return apiResponseBuilder(200, $result);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserReview  $userReview
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, UserReviewValidator $validation, StoreUserReviewService $storeUserReviewService)
    {
        $data = $request->json()->all();
        $validator = $validation->validate($data);

        if ($validator === true) {
            $result = $storeUserReviewService->call($data, $id);
            if ($result['status']) {
                return apiResponseBuilder(200, $result);
            } else {
                return $this->apiInternalServerErrorResponse($result['message']);    
            }
        } else {
            return $this->apiUnprocessableEntityResponse($validator->all());    
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserReview  $userReview
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result =  $this->repository->findById($id)->delete();

        if ($result) {
            return apiResponseBuilder(200, ['status' => true, 'message' => trans('message.delete_user_review_success')]);
        } else {
            return $this->apiInternalServerErrorResponse(trans('message.delete_user_review_failed'));
        }
    }
}
