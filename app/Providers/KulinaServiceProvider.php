<?php

namespace App\Providers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;

class KulinaServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        DB::disableQueryLog();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->repositories();
    }

    /**
     * Register all repositories service
     */
    private function repositories()
    {
        $this->app->bind('App\Domain\UserReview\UserReviewRepositoryInterface', 'App\Infrastructure\Data\Persistence\DbUserReviewRepository');
    }

}