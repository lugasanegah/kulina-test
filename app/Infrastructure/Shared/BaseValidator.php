<?php

namespace App\Infrastructure\Shared;

use Illuminate\Support\Facades\Validator;

abstract class BaseValidator
{
    /**
     * Default rules
     *
     * @var array
     */
    protected $rules = [];

    /**
     * Default messages
     *
     * @var array
     */
    protected $messages = [];

    /**
     * @param array $request
     * @return bool
     */
    public function validate($request = [])
    {
        $validation = Validator::make($request, $this->rules, $this->messages);
        if ($validation->passes()) {
            return true;
        }
        return $validation->errors();
    }
}
